

import UIKit
import ObjectMapper

class CheckMessageModel: GeneralResponseModel {
    
    var data: [CheckMessage]?

    override func mapping(map: Map) {
        data <- map
        
    }
}

class CheckMessage: Mappable {
    
    var success:Bool=false

    var flag:Int=0
      var id:Int=0
   

    required init?(map: Map){
        
    }

    func mapping(map: Map) {
        id <- map["id"]
        flag <- map["flag"]
        success <- map["success"]



    }
    
    
}
