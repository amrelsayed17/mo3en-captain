

import UIKit
import BFPaperButton
import MapKit
import CoreLocation

class Home: UIViewController,MKMapViewDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate  {
    @IBOutlet weak var mysegment: UISegmentedControl!

    
    
    @IBOutlet weak var my_map: MKMapView!
    var locationManager:CLLocationManager = CLLocationManager()
       var currentLocation:CLLocation?
       var pickedLocation:CLLocationCoordinate2D?
    @IBOutlet weak var view_undernumber: BFPaperButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        editViews()
        

    }
    
    
    
    func editViews()
     {
        
        mysegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 50/255, green: 96/255, blue: 155/255, alpha: 1)], for: .selected)
             

        
        
         
        my_map.delegate = self


                 self.locationManager.distanceFilter  = 1000
                 self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
                 self.locationManager.delegate = self
                 self.locationManager.requestWhenInUseAuthorization()
                 
                 let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(handleTap))
                 gestureRecognizer.delegate = self
                 my_map.addGestureRecognizer(gestureRecognizer)
                 my_map.showsUserLocation=true
                 my_map.showsCompass=true
                 my_map.showsScale=true
                 my_map.showsTraffic=true
        
        
         CustomViews.editMybutton(theview: view_undernumber, border: 1, radius: 20, color: UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1))
    }
          func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
                 print("didChangeAuthorizationStatus \(status)")
                 
                 if status == .authorizedWhenInUse  {
                     print(".Authorized")
                     self.locationManager.startUpdatingLocation()
                 }
             }
             func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
                 
                 currentLocation = locations.last
                 if currentLocation != nil{
                     print("there is location ")
                     centerMapOnLocation(currentLocation!)
                 }
                 
             }
             
             func centerMapOnLocation(_ location: CLLocation) {
                 let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                           latitudinalMeters: Double(5) * 2.0, longitudinalMeters: Double(5) * 2.0)
                 my_map.setRegion(coordinateRegion, animated: true)
             }
             
             @objc func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
                 
                 my_map.removeAnnotations(my_map.annotations)
                 let location = gestureReconizer.location(in: my_map)
                 let coordinate = my_map.convert(location,toCoordinateFrom: my_map)
                 
    //          // Add annotation:
    //              let annotation = MKPointAnnotation()
    //              annotation.coordinate = coordinate
    //              my_map.addAnnotation(annotation)
    //              my_lat=coordinate.latitude
    //              my_lan=coordinate.longitude
    //              print(coordinate.latitude,",",coordinate.longitude)
    //             //dress_map.text=annotation.title
    //             getAddressFromLatLon(pdblLatitude: "\(my_lat)", withLongitude: "\(my_lan)")


             }



}
