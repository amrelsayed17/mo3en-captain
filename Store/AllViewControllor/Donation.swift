

import UIKit

class Donation: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var my_table: UITableView!

    @IBOutlet weak var view_underserach: UIView!
    
    @IBOutlet weak var search_tf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editView()

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DonationCell=tableView.dequeueReusableCell(withIdentifier: "DonationCell", for: indexPath) as! DonationCell
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    
    func editView()
    {
        CustomViews.editMybutton(theview: view_underserach, border: 1, radius: 16, color: UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1))
        
        search_tf.setPlaceHolderColor(color: UIColor(red: 133/255, green: 133/255, blue: 133/255, alpha: 1))
        


        
        
    }


}
