

import UIKit
import ObjectMapper

class GetAllBankModelList: GeneralResponseModel {
    
    var data: [AllBanks]?

    override func mapping(map: Map) {
        data <- map
        
    }
}

class AllBanks: Mappable {
    

    

    var success:Bool?
    var id:String?
    var bank_name:String?
    var account_name:String?
    var account_number:String?
    var iban_number:String?

   

    required init?(map: Map){
        
    }

    func mapping(map: Map) {
        success <- map["success"]
        id <- map["id"]

        bank_name <- map["bank_name"]
        account_name <- map["account_name"]
        account_number <- map["account_number"]
        iban_number <- map["iban_number"]

    }
    
}
