

import UIKit

class YourPoint: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var my_table: UITableView!
    @IBOutlet weak var view_undergive: UIView!
    @IBOutlet weak var view_underpoint: UIView!

    

    override func viewDidLoad() {
        super.viewDidLoad()
        editView()
        

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:YourPointCell=tableView.dequeueReusableCell(withIdentifier: "YourPointCell", for: indexPath) as! YourPointCell
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    func editView()
    {
        CustomViews.editMybutton(theview: view_undergive, border: 1, radius: 4, color: UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1))
        
        CustomViews.editMybutton(theview: view_underpoint, border: 1, radius: 4, color: UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1))


        
        
    }

}
