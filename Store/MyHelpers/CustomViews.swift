//
//  CustomViews.swift
//  ElNoranyaParent
//
//  Created by amr gamal on 9/8/19.
//  Copyright © 2019 Amr Elsayed. All rights reserved.
//

import UIKit

class CustomViews: NSObject {

    static var mainColor:UIColor=UIColor(red: 108/255, green: 32/255, blue: 178/255, alpha: 1)
    static var brownColor:UIColor=UIColor(red: 183/255, green: 163/255, blue: 50/255, alpha: 1)

    static func editMybutton(theview:UIView,border:Int,radius:Float,color:UIColor)
    {
        theview.layer.borderWidth = CGFloat(border)
        theview.layer.masksToBounds = false
        theview.layer.borderColor = color.cgColor
        theview.layer.cornerRadius = CGFloat(radius)
        theview.clipsToBounds = true
    }
}
