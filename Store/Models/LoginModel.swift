

import UIKit
import ObjectMapper


class LoginModel:  GeneralResponseModel {

    
    var data: TokenData?

    

    override func mapping(map: Map) {
        message <- map["message"]
        status <- map["status"]
        data <- map["data"]



    }
    
}

class TokenData: Mappable {
    
    required init?(map: Map) {
    }
    
    var api_token: String?
    var userdata: UserDataInformation?
    init() {
        
    }
    
    func mapping(map: Map) {
        
       
        api_token <- map["token"]
        userdata <- map["user"]
    }
    
}
class UserDataInformation : NSObject , Mappable , NSCoding {
    
    required init?(map: Map) {
    }
    

    

    var id: Int=0
    var name: String=""
    var user_name: String=""
    var phone: String=""
    var email: String=""
    var nationality: String=""
    var d_o_b: String=""
    var gender: String=""
    var status: String=""
    var rate: String=""

    var transport_photo: String=""
    var photo: String=""
    var points: Int=0
    var points_in_pounds: Int=0
    var last_date_to_change_points: String=""
    var wallet_balance: Int=0
    var accept_privacy: String=""

    var user_token: String=""




    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        user_name <- map["user_name"]
        phone <- map["phone"]
        email <- map["email"]
        nationality <- map["nationality"]
        d_o_b <- map["d_o_b"]
        gender <- map["gender"]
        status <- map["status"]
        rate <- map["rate"]
        
        transport_photo <- map["transport_photo"]
        photo <- map["photo"]
        points <- map["points"]
        points_in_pounds <- map["points_in_pounds"]
        last_date_to_change_points <- map["last_date_to_change_points"]
        wallet_balance <- map["wallet_balance"]
        accept_privacy <- map["accept_privacy"]
   
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? Int ?? 0
        let name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        let user_name = aDecoder.decodeObject(forKey: "user_name") as? String ?? ""
        let phone = aDecoder.decodeObject(forKey: "phone") as? String ?? ""
        let email = aDecoder.decodeObject(forKey: "email") as? String ?? ""
       let nationality = aDecoder.decodeObject(forKey: "nationality") as? String ?? ""
        let d_o_b = aDecoder.decodeObject(forKey: "d_o_b") as? String ?? ""
        let gender = aDecoder.decodeObject(forKey: "gender") as? String ?? ""
        let status = aDecoder.decodeObject(forKey: "status") as? String ?? ""
        
        
        let rate = aDecoder.decodeObject(forKey: "rate") as? String ?? ""
        let transport_photo = aDecoder.decodeObject(forKey: "transport_photo") as? String ?? ""
        let photo = aDecoder.decodeObject(forKey: "photo") as? String ?? ""
        let points = aDecoder.decodeObject(forKey: "points") as? Int ?? 0
        let points_in_pounds = aDecoder.decodeObject(forKey: "points_in_pounds") as? Int ?? 0
        
        let last_date_to_change_points = aDecoder.decodeObject(forKey: "last_date_to_change_points") as? String ?? ""
         let wallet_balance = aDecoder.decodeObject(forKey: "wallet_balance") as? Int ?? 0
         let accept_privacy = aDecoder.decodeObject(forKey: "accept_privacy") as? String ?? ""
        let user_token = aDecoder.decodeObject(forKey: "user_token") as? String ?? ""

        self.init(id:id,name:name,user_name:user_name ,email:email,phone:phone
            ,nationality:nationality,d_o_b:d_o_b,gender:gender,rate:rate,status:status, transport_photo:transport_photo, photo:photo,points:points,points_in_pounds:points_in_pounds,last_date_to_change_points:last_date_to_change_points, wallet_balance:wallet_balance,accept_privacy:accept_privacy,user_token:user_token)
    }
    init( id:Int,name:String,user_name:String ,email:String,phone:String ,  nationality:String,d_o_b:String,gender:String,rate:String,status:String,transport_photo:String,photo:String,points:Int,points_in_pounds:Int,last_date_to_change_points:String,wallet_balance:Int,accept_privacy:String,user_token:String) {
        
        self.id=id
        self.name=name
        self.user_name=user_name
        self.phone=phone
        self.email=email
        self.nationality=nationality
        self.d_o_b=d_o_b
        self.gender=gender
        self.rate=rate
        self.status=status
        self.transport_photo=transport_photo
        self.photo=photo
            self.points=points
            self.points_in_pounds=points_in_pounds
            self.last_date_to_change_points=last_date_to_change_points
            self.wallet_balance=wallet_balance
            self.accept_privacy=accept_privacy
    
     self.user_token=user_token

    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name,    forKey: "name")
        aCoder.encode(user_name,    forKey: "user_name")
        aCoder.encode(email,    forKey: "email")
        aCoder.encode(phone,    forKey: "phone")
        aCoder.encode(nationality,    forKey: "nationality")
        aCoder.encode(d_o_b,    forKey: "d_o_b")
        aCoder.encode(gender,    forKey: "gender")
        aCoder.encode(rate,    forKey: "rate")
        aCoder.encode(transport_photo,    forKey: "transport_photo")
        aCoder.encode(status,    forKey: "status")
        aCoder.encode(photo,    forKey: "photo")
        aCoder.encode(points,    forKey: "points")
        
          aCoder.encode(points_in_pounds,    forKey: "points_in_pounds")
          
          aCoder.encode(last_date_to_change_points,    forKey: "last_date_to_change_points")
          aCoder.encode(wallet_balance,    forKey: "wallet_balance")
          aCoder.encode(accept_privacy,    forKey: "accept_privacy")
        
        aCoder.encode(user_token,    forKey: "user_token")

    }
    
    override init() {
        
    }
}
