

import UIKit

class Login: UIViewController {

    @IBOutlet weak var view_undername: UIView!
    @IBOutlet weak var view_underpassword: UIView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editViews()

    }
    
    
    
    
    
    
    
    
    
    func editViews()
    {
        name.setPlaceHolderColor(color: UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1))

        
        password.setPlaceHolderColor(color: UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1))

        
        CustomViews.editMybutton(theview: view_undername, border: 1, radius: 11, color: UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1))
        
          CustomViews.editMybutton(theview: view_underpassword, border: 1, radius: 11, color: UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1))
        
    }



}

extension UITextField{
    
    func setPlaceHolderColor(color:UIColor){
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
}
