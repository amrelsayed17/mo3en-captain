

import UIKit
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        Thread.sleep(forTimeInterval: 4);

        
        if(Defaults.isUserLogin())
               {
                   LogHelper.StartMainHome()
               }
        
//        FirebaseApp.configure()
//
//        Messaging.messaging().delegate = self
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//        
//        application.registerForRemoteNotifications()
        
        return true
    }

   // [START receive_message]
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            print(userInfo)
        }
        
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            // Print full message.
            print(userInfo)
            
            completionHandler(UIBackgroundFetchResult.newData)
        }
        // [END receive_message]
        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            print("Unable to register for remote notifications: \(error.localizedDescription)")
        }
        
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            print("APNs token retrieved: \(deviceToken)")
            
        }
        
        func applicationWillResignActive(_ application: UIApplication) {
            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
            // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        }
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
            // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        }
        
        
    }


    // [START ios_10_message_handling]
    @available(iOS 10, *)
    extension AppDelegate : UNUserNotificationCenterDelegate {
        
        // Receive displayed notifications for iOS 10 devices.
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    willPresent notification: UNNotification,
                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            let userInfo = notification.request.content.userInfo
            print("Notification: \(userInfo)")
            
            showNotificationDialog(aps: userInfo["aps"]! as! NSDictionary)
            completionHandler([])
            
    //        if let offer_id:String = userInfo["offer_id"] as? String {
    //            showNotificationDialog(aps: userInfo["aps"]! as! NSDictionary, notification:["offer_id":offer_id])
    //            completionHandler([])
    //        }
    //        else if let order_id:String = userInfo["order_id"] as? String {
    //            showNotificationDialog(aps: userInfo["aps"]! as! NSDictionary, notification:["order_id":order_id])
    //            completionHandler([])
    //        }
            
        }
        
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            let userInfo = response.notification.request.content.userInfo
            print("Notification: \(userInfo)")
            
            showNotificationDialog(aps: userInfo["aps"]! as! NSDictionary)
            completionHandler()
    //
    //        if let offer_id:String = userInfo["offer_id"] as? String {
    //            showNotificationDialog(aps: userInfo["aps"]! as! NSDictionary, notification:["offer_id":offer_id])
    //            completionHandler()
    //        }
    //        else if let order_id:String = userInfo["order_id"] as? String {
    //            showNotificationDialog(aps: userInfo["aps"]! as! NSDictionary, notification:["order_id":order_id])
    //            completionHandler()
    //        }
            
        }
        

        //,notification:[String:Any]
        func showNotificationDialog(aps:NSDictionary)  {
            
            let title:String = (aps["alert"] as! NSDictionary)["title"]  as? String ?? ""
            let body:String = (aps["alert"] as! NSDictionary)["body"]  as? String ?? ""
            AlertMessages.showMessage(title: title, body: body, theme: .success)
            
    //        if((notification["offer_id"] as? String ?? "") != "")
    //        {
    //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    //            let main_page_navigation = mainStoryBoard.instantiateViewController(withIdentifier: "AdvertsDetails") as! AdvertsDetails
    //            main_page_navigation.order_id=Int(notification["offer_id"] as? String ?? "0") ?? 0
    //            let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //            appDelegate.window?.rootViewController = main_page_navigation
    //        }
    //        else if((notification["order_id"] as? String ?? "") != "")
    //        {
    //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    //            let main_page_navigation = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetails") as! OrderDetails
    //            main_page_navigation.order_number=Int(notification["order_id"] as? String ?? "0") ?? 0
    //            let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //            appDelegate.window?.rootViewController = main_page_navigation
    //        }
            
        }
        
    }
//    // [END ios_10_message_handling]
//
//    extension AppDelegate : MessagingDelegate {
//        // [START refresh_token]
//        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//            print("Firebase registration token: \(fcmToken)")
//            
//        }
//        
//        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//            print("Received data message: \(remoteMessage.appData)")
//        }
//        // [END ios_10_data_message]
//    }
//

