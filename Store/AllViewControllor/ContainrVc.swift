

import UIKit
import PopupDialog

class ContainrVc: UIViewController {

    enum TabBarStatus {
           case Home
           case Commission
        case Addproduct

           case CallUs
           case Profile
       }
       
       static var currentTabBarStatus:TabBarStatus=TabBarStatus.Home
    
    
    @IBOutlet weak var icon_1: UIImageView!
     @IBOutlet weak var name_1: UIButton!
     
     @IBOutlet weak var icon_2: UIImageView!
     @IBOutlet weak var name_2: UIButton!
     
     @IBOutlet weak var icon_3: UIImageView!
     @IBOutlet weak var name_3: UIButton!
    
    @IBOutlet weak var icon_4: UIImageView!
    @IBOutlet weak var name_4: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        icon_1.image = icon_1.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        icon_2.image = icon_2.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        icon_3.image = icon_3.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        icon_4.image = icon_4.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)

        
    
        
        
        
        
        
        updateStatus()
        NotificationCenter.default.addObserver(self, selector: Selector(("updateStatus")), name: NSNotification.Name(rawValue: "MessageReceived"), object: nil)

        
        
    }
    
    
    @objc  func updateStatus()  {
         //   print("currentTabBarStatus",ContainerVc.currentTabBarStatus)
            if(ContainrVc.currentTabBarStatus == .Home)
            {
               icon_1.tintColor=CustomViews.mainColor
                icon_2.tintColor=UIColor.darkGray
                icon_3.tintColor=UIColor.darkGray
                icon_4.tintColor=UIColor.darkGray


            }
            else if(ContainrVc.currentTabBarStatus == .Commission)
            {
                icon_2.tintColor=CustomViews.mainColor
                icon_1.tintColor=UIColor.darkGray
                icon_3.tintColor=UIColor.darkGray
                icon_4.tintColor=UIColor.darkGray

              
            }
                else if(ContainrVc.currentTabBarStatus == .CallUs)
                        {
                            icon_3.tintColor=CustomViews.mainColor
                            icon_1.tintColor=UIColor.darkGray
                            icon_2.tintColor=UIColor.darkGray
                            icon_4.tintColor=UIColor.darkGray

                            
                        }
                
            else if(ContainrVc.currentTabBarStatus == .Profile)
            {
                icon_4.tintColor=CustomViews.mainColor
                icon_1.tintColor=UIColor.darkGray
                icon_2.tintColor=UIColor.darkGray
                icon_3.tintColor=UIColor.darkGray

                
            }
        else if(ContainrVc.currentTabBarStatus == .Addproduct)
               {
                   icon_4.tintColor=UIColor.darkGray
                   icon_1.tintColor=UIColor.darkGray
                   icon_2.tintColor=UIColor.darkGray
                   icon_3.tintColor=UIColor.darkGray

                   
               }
    }
    
    
    
    @IBAction func clickToProfile(_ sender: Any) {
       if (Defaults.isUserLogin() == false)
                                    {
                                //        showCartLockDialog(animated: true)

                                    }
                         else{
                             
                             LogHelper.goToViewController(identifier: "Profile", parentVC: self)

                         }
    }
    
    
    @IBAction func ClickToCallUs(_ sender: Any) {

   
                  
                  LogHelper.goToViewController(identifier: "CallUs", parentVC: self)

              
          }
    
    
    
//
//              func showCartLockDialog(animated: Bool = true) {
//
//                           let ratingVC = LockCart(nibName: "LockCart", bundle: nil)
//
//
//                           let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true){
//
//
//                           }
//
//                           popup.view.backgroundColor=UIColor.clear
//
//                           present(popup, animated: animated, completion: nil)
//
//                       }
    
    
    
    
    
    @IBAction func clickToGetCommesion(_ sender: Any) {
        
        

        LogHelper.goToViewController(identifier: "Commission", parentVC: self)
                                        
                                    }
        
 
       

    @IBAction func clickToAddProduct(_ sender: Any) {
        if (Defaults.isUserLogin() == false)
                             {
                           //      showCartLockDialog(animated: true)

                             }
                  else{
                      
                      LogHelper.goToViewController(identifier: "AddProduct", parentVC: self)

                  }
    }
    
    
    
    
    
    @IBAction func clickToHome(_ sender: Any) {
        LogHelper.goToViewController(identifier: "Home", parentVC: self)

        
    }
}
