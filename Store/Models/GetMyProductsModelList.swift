

import UIKit
import ObjectMapper


class GetMyProductsModelList: GeneralResponseModel {
    
    var data: [MyProducts]?

    override func mapping(map: Map) {
        data <- map
        
    }
}

class MyProducts: Mappable {
   


    var success:Bool?
    var id:String?
    var name:String?
    var price:String?
    var img:String?
    var description:String?
    
    var category_id:String?
    var category_name:String?
    var city_id:String?
    var city:String?
    var favorite:String?
    var date_reg:String?


   

    required init?(map: Map){
        
    }

    func mapping(map: Map) {
        success <- map["success"]
        id <- map["id"]

        name <- map["name"]
        price <- map["price"]
        img <- map["img"]
        description <- map["description"]
        
        category_id <- map["category_id"]
        category_name <- map["category_name"]
        city_id <- map["city_id"]
        city <- map["city"]

        favorite <- map["favorite"]
          date_reg <- map["date_reg"]
    }
    
}

