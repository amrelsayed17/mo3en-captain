

import UIKit


class CaseData: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var my_table: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let TempImageView = UIImageView(image: UIImage(named: "background1"))
               TempImageView.frame = self.my_table.frame
               self.my_table.backgroundView = TempImageView

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CaseDataCell=tableView.dequeueReusableCell(withIdentifier: "CaseDataCell", for: indexPath) as! CaseDataCell
        cell.editView()
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 658
    }

    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
