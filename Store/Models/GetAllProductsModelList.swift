

import UIKit
import ObjectMapper


class GetAllProductsModelList:GeneralResponseModel {
    
    var data: [AllProducts]?

    override func mapping(map: Map) {
        data <- map
        
    }
}

class AllProducts: Mappable {
    

    

    var success:Bool?
    var id:String?
    var name:String?
    var price:String?
     var img:String?
    var img2:String?
     var img3:String?
    var img4:String?
     var img5:String?
    var description:String?
     var category_id:String?
    var category_name:String?
     var user_id:String?
    var user_name:String?
     var user_phone:String?
    var city:String?
     var date_reg:String?
   

   

    required init?(map: Map){
        
    }

    func mapping(map: Map) {
        success <- map["success"]
        id <- map["id"]

        name <- map["name"]
        price <- map["price"]
             img <- map["img"]

             img2 <- map["img2"]
        img3 <- map["img3"]
             img4 <- map["img4"]

             img5 <- map["img5"]
        description <- map["description"]
             category_id <- map["category_id"]

             category_name <- map["category_name"]
        user_name <- map["user_name"]
             user_phone <- map["user_phone"]

             city <- map["city"]
        date_reg <- map["date_reg"]

 

    }
    
}
