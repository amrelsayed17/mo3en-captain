

import UIKit
import ObjectMapper


class GetCategoryModelList: GeneralResponseModel {
    
    var data: [AllCategory]?

    override func mapping(map: Map) {
        data <- map
        
    }
}

class AllCategory: Mappable {
    

    

    var success:Bool?
    var id:String?
    var name:String?
    var is_selected:Bool?

    
    
    
   

   

    required init?(map: Map){
        
    }

    func mapping(map: Map) {
        success <- map["success"]
        id <- map["id"]

        name <- map["name"]
 

    }
    
}
