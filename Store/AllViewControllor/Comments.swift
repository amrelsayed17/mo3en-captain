

import UIKit

class Comments: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var my_table: UITableView!
    @IBOutlet weak var view_undercomment: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomViews.editMybutton(theview: view_undercomment, border: 1, radius: 8, color: UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1))

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CommentsCell=tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! CommentsCell
         cell.editView()
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }

}
