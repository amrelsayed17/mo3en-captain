

import UIKit

class refuseAndCancelOperation: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var my_table: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:refuseAndCancelOperationCell=tableView.dequeueReusableCell(withIdentifier: "refuseAndCancelOperationCell", for: indexPath) as! refuseAndCancelOperationCell
           cell.editView()
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }

}
